#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace cv;
using namespace std;

#define RES 4
#define N 20
#define START 17
#define INTERVAL 1
#define INPUT_IMG "img_gather"


int main( int argc, char** argv ) {

	char fname [256];
	Mat src;
	
	/// open webcam
	VideoCapture cam_stream(0);	// 640 x 480
	//~ cam_stream.set(CAP_PROP_EXPOSURE, 400);
	if(!cam_stream.isOpened()) {
		return -1;
	}

	/// gather pictures

	for (int i = START; i < N+RES+START; i++) {
		cam_stream >> src;
		imshow("cam", src);
		waitKey(INTERVAL);
		if (i<RES+START) {
			continue;
		}
		sprintf(fname, "../%s/%d.jpg",INPUT_IMG, i-RES);
		imwrite(fname, src);
	}
	return(0);
}
