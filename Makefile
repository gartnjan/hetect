CC = g++
CFLAGS = -g -Wall
SRCS = main.cpp corners.cpp radial.cpp utils.cpp chessboard.cpp locate.cpp
PROG = main

MATH = -lm
OPENCV = `pkg-config opencv --cflags --libs`
LIBS = $(OPENCV) $(MATH)

$(PROG):$(SRCS)
	$(CC) $(CFLAGS) -o $(PROG) $(SRCS) $(LIBS)
	
