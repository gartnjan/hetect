#include "hetect.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>

using namespace cv;
using namespace std;

// TO-DO list
/// adaptivni threshold pro pocet rohu
/// podivat do whyconu na test elipsovitosti, pripadne udelat test spojistosti a stredova snetrie
/// na vzoru i v konv. masce ostrejsi scipky misto ctvrtin
/// zvetsin nejmensi polomery a zkusit prisnejsi rozsah cerna-bila
/// vztahnout limit elispovitiosti na delku paprsku

/// nehledat paprsky na sedivych mistech
/// korelace mximum na stejnych mistech
/// vzit velikost skoku z prechodu an konvolcni srovnani
/// dorozmazat pri delani paprsku

/// zkusit korelaci 2S sachovnice kolem stredu a spresnit tim stred obrazc
/// nechat dobehnout aspon na median, treba na 3/4

Source_t source;
Mat src_gray, src;
int MASKING = false;
float target_radius;
ofstream logout;

int main( int argc, char** argv ) {

	/// arguments parsing	
	int source_set = false;
	int target_set = false;
	int resize_en = false;
	if(cmdOptionExists(argv, argv+argc, "-w")) {
		source = Webcam;
		source_set = true;
	}
	if(cmdOptionExists(argv, argv+argc, "-f")) {
		source = Folder;
		source_set = true;
	}
	if (!source_set) {
		cout << "set source -f for folder or -w for webcam\n";
		return 1;
	}
	if(cmdOptionExists(argv, argv+argc, "-m")) {
		MASKING = true;
	}
	if(cmdOptionExists(argv, argv+argc, "-a5")) {
		target_radius = 0.074;
		target_set = true;
	}
	if(cmdOptionExists(argv, argv+argc, "-a4")) {
		target_radius = 0.147;
		target_set = true;
	}
	if(cmdOptionExists(argv, argv+argc, "-a3")) {
		target_radius = 0.22;
		target_set = true;
	}
	if (!target_set) {
		cout << "set target radius as -a5, -a4 or -a3\n";
		return 1;
	}
	if(cmdOptionExists(argv, argv+argc, "-r")) {
		resize_en = true;
	}

	/// webcam input
	if (source == Webcam) {
		char source_window[] = "Source image";

		VideoCapture cam_stream(0);	// 640 x 480
		if(!cam_stream.isOpened()) {
			return -1;
		}

		namedWindow( source_window, CV_WINDOW_AUTOSIZE );
		logout.open ("img_output/log_cam.txt", ios::out);
		while(1) {
			cam_stream >> src;
			//~ resize(src, src,Size(0,0), 0.8, 0.8);
			cvtColor( src, src_gray, CV_BGR2GRAY);
			blur( src_gray, src_gray, Size(4,4));
			find_corners_shi();
			Mat detected_edges;
			resize(src, src,Size(0,0), 2.1, 2.1);
			imshow( source_window, src);
			waitKey(1);
		}

		/// browse a file
	} else if(source == Folder) {
		DIR *dir;
		struct dirent *ent;
		string fname, path;
		vector<string> sorted_files;
		if ((dir = opendir ("img_input/")) != NULL) {
			system("exec rm -r img_output/*");
			while ((ent = readdir (dir)) != NULL) {
				fname = ent->d_name;
				if (fname[0] != '.') {
					sorted_files.push_back(fname);
				}
			}
			closedir (dir);
			logout.open ("img_output/log.txt", ios::out);
			logout << "fname ,detected,  near_mode, harris_time, rest_time, total_time, corn_lim, tar_x, tar_y, tar_z\n";
			//~ sort(sorted_files.rbegin(), sorted_files.rend());
			sort(sorted_files.begin(), sorted_files.end());
			for(size_t c=0; c<sorted_files.size(); c++) {
				fname = sorted_files[c];
				path = "img_input/"+fname;
				logout << fname << ", ";
				cout << "\n\n"<< fname << "\n";
				src = imread(path);
				if (resize_en)
					resize(src, src,Size(0,0), 0.7, 0.7);
				//~ resize(src, src,Size(0,0), 0.2, 0.2);
				cvtColor( src, src_gray, CV_BGR2GRAY);
				blur( src_gray, src_gray, Size(4,4) );
				//~ blur( src, src, Size(3,3) );
				find_corners_shi();
				path = "img_output/"+fname;
				imwrite(path, src);
			}

		}
	}
	logout.close();
	return(0);
}

char* getCmdOption(char ** begin, char ** end, const std::string & option) {
	char ** itr = std::find(begin, end, option);
	if (itr != end && ++itr != end) {
		return *itr;
	}
	return 0;
}

bool cmdOptionExists(char** begin, char** end, const std::string& option) {
	return std::find(begin, end, option) != end;
}
