#include "hetect.hpp"
#include <stdio.h>
#include <stdlib.h>

//~ #define DEBUG 1
extern int AROUND_STEPS;
extern Mat src_gray, src;

int radial_check(Point2f corner, RotatedRect& min_ellipse, int conv_shift, int color_range, int biggest_chess) {
	float steps_limit = 0.5; /// how many strips must filfil continuity test (of all possible distances)
	float dmp = 0.85; /// pseudomedian from this position used for min_dist counting
	float min_dist_coeff = 0.3; /// fraction of pseudomedian used as min_dist for strips test
	float center_tolerance = 0.05;
	float on_ellipse_limit = 0.9; /// how many valid points (off recieved distances) must lioe on ellipse
	float distances_limit = 0.8; /// how many critical distances must be returned by get_critical_distances of all possible
	//~ printf("\nrange %d \n ", color_range);
	/// get critical distances using strip method
	vector<int> critical_distances;
	int min_dist = biggest_chess;
	int possible_distances = 0; /// how many critical distances could be get, if all readable
	get_critical_distances(critical_distances, corner, conv_shift, min_dist, &possible_distances, color_range, true);

	min_dist = count_median(critical_distances, dmp)*min_dist_coeff;
	vector<Point2f> critical_points = get_critical_distances(critical_distances, corner, conv_shift, min_dist, &possible_distances, color_range, false);
	int distances = critical_distances.size() ;
	//~ printf("got %d critical distances\n", distances);
	if (distances < possible_distances*distances_limit)
		return false;
	/// aply continuity filter to choose which point are valid
	vector<int8_t> critical_point_valid;
	int start_at = 0;
	int points_ok = continuity_filter(critical_distances, critical_point_valid,&start_at);
	#ifdef DEBUG
	printf("%d of %1.2f conrinuoly valid \n", points_ok, possible_distances*steps_limit);
	#endif
	//~ printf("ok %d\n", points_ok);
	if (points_ok < possible_distances*steps_limit){
		#ifdef DEBUG
		printf("druhe kolo continuity\n");
		#endif
		points_ok = continuity_filter(critical_distances, critical_point_valid,&start_at);
		if (points_ok < possible_distances*steps_limit){
			#ifdef DEBUG
			//~ printf("invalid continuity, only %d from %f\n", points_ok, distances*steps_limit);
			#endif
			return false;
		}
	}
	/// make ellipse aproximation of valid points and get critical distances on ellipse
	vector<Point2f> valid_critical_points;
	vector<Point2f> testable_critical_points;
	vector<uint> valid_critical_distances;

	for (size_t i = 0; i < critical_distances.size(); i++){
		if(critical_point_valid[i] >= 0){
			testable_critical_points.push_back(critical_points[i]);
			if (critical_point_valid[i]){
				valid_critical_points.push_back(critical_points[i]);
				valid_critical_distances.push_back(critical_distances[i]);
				#ifdef DEBUG
				Vec3b red(200,200,2);
				//~ circle( src, critical_points[i], 1, red, 1, 0 );
				//~ src.at<Vec3b>(critical_points[q][i]) = red;
				#endif
			}
		}
	}

	vector<Point2f> new_valid_points;
	int points_on_ellipse = ellipse_test(valid_critical_points, testable_critical_points, min_ellipse, 14, new_valid_points);
	int size = (min_ellipse.size.width + min_ellipse.size.height) /4;
	if (size > 20)
		on_ellipse_limit *= 0.9;
	if(points_on_ellipse > distances*on_ellipse_limit/2)
		points_on_ellipse = ellipse_test(new_valid_points, testable_critical_points, min_ellipse, 10, new_valid_points);
	#ifdef DEBUG
	printf("%d/%d points on ellipse\n", points_on_ellipse, (int)(distances*on_ellipse_limit));
	# endif
	//~ printf("mistake %f\n", ellipse_mistake);
	int center_ok = abs(corner.x - min_ellipse.center.x) <= center_tolerance*min_ellipse.size.width && abs(corner.y - min_ellipse.center.y) <= center_tolerance*min_ellipse.size.height;
	#ifdef DEBUG
	if(!center_ok)
		printf("propadl tstem stredu\n");
	# endif
	//~ printf(" center %d \n", center_ok);
	return points_on_ellipse >= distances*on_ellipse_limit && center_ok;
}


int ellipse_test(vector<Point2f> valid_critical_points, vector<Point2f> points_to_test, RotatedRect& min_ellipse, float ellipse_limit, vector<Point2f>& new_valid_points ){
	int minimal_ellipse_size = 10;
	min_ellipse = fitEllipse(Mat(valid_critical_points));
	#ifdef DEBUG
	int second = false;
	if (new_valid_points.size() > 0){
		second = true;
		ellipse( src, min_ellipse, Scalar(0,255,255), 1, 8 );
	}
	#endif
	/// points on ellipse 
	int a = min_ellipse.size.width /2;
	int b = min_ellipse.size.height /2;
	float theta = min_ellipse.angle/360*2*M_PI;
	int x0 = min_ellipse.center.x;
	int y0 = min_ellipse.center.y;
	int points_on_ellipse = 0;
	if (a < minimal_ellipse_size || b < minimal_ellipse_size){
		#ifdef DEBUG
		printf("TOO SMALL size %d %d\n", a,b);
		#endif
		return 0;
	}
	for (size_t step = 0; step < points_to_test.size(); step++) {
		int x = points_to_test[step].x;
		int y = points_to_test[step].y;
		double act_error = abs( pow(( (x-x0)*cos(theta)+(y-y0)*sin(theta) ),2) /(a*a) + pow(( (x-x0)*sin(theta)-(y-y0)*cos(theta) ),2)/(b*b) - 1);
		if (act_error*(a+b)  <= ellipse_limit){
			points_on_ellipse ++;
			new_valid_points.push_back(points_to_test[step]);
		} else {
			#ifdef DEBUG
			Vec3b red(20,20,200);
			if (second)
				circle( src, points_to_test[step], 1, red, 3, 0 );
			//~ src.at<Vec3b>(critical_points[q][i]) = red;
			#endif
		}
		//~ printf("%.2f ",act_error*(a+b));
	}
	#ifdef DEBUG
	printf("size %d %d, ", a,b);
	# endif
	
	return points_on_ellipse;
}


vector<Point2f>  get_critical_distances(vector<int>& critical_distances, Point2f corner, int conv_shift, int min_dist, int* possible_distances, int color_range, int firstrun) {
	int start_dist = 5; /// how far from center start to drawing, biggest possible, cant find ellipse slower than this
	int steps = AROUND_STEPS; /// dont change this, it is used also in fction  radial_check()
	int batch_size = 3; /// how many pixels it sums  while detecting ellipse edge
	int range_coeff = color_range/4; /// how strong must be brightness increase compared to last batch range
	int skip_size = 1; /// look on not every pixel in a row
	int steps_gap = 3; /// for vzor_8, gap on the beggining
	int steps_gap2 = 3; /// for vzor_8, gap on the end of quartal
	float blockation_limit = 0.2; /// how much can vary opposite radiuses while detecting elipse edge
	int max_dist;

	if(!firstrun){
		max_dist = 6*min_dist;
	} else {
		max_dist = 0xFFFFFFF;
	}
	critical_distances.clear();
	vector<vector<Point2f>> critical_points_temp(4);
	vector<vector<int>>critical_distances_temp(4);
	
	//~ printf("%d\n", conv_shift);
	/// angle iterate
	int last_in_gap = false;
	uint8_t border_c = 0;
	*possible_distances =0;
	for (int step = 0; step < steps/2.; step++) {
		int step_temp = step;
		while (step_temp >= steps/4.){
			step_temp -= steps/4.;
		}
		if (step_temp <= steps_gap || step_temp >= steps/4. -steps_gap2){
		//~ if (step_temp <= steps_gap){
			/// save in which quadrant we are
			if (!last_in_gap){
				border_c  = (border_c +1) % 4; 
			}
			last_in_gap = true;
			continue;
		} else {
			last_in_gap = false;
		}
		*possible_distances = *possible_distances+2;
		int distance[2] = {start_dist-skip_size, start_dist-skip_size};
		float ang[2];
		ang[0] = 2*M_PI / steps * ((step+conv_shift)%steps);
		ang[1] = ang[0] + M_PI; /// point on the oposite side, radiuses should be simetric
		int index_out;
		vector<Point2f> opposite_points(2);
		opposite_points[0] = get_point_on_circle(corner, ang[0], distance[0], &index_out);
		opposite_points[1] = get_point_on_circle(corner, ang[1], distance[1], &index_out);
		float floating_average[2];
		floating_average[0] = src_gray.at<uchar>(opposite_points[0]);
		floating_average[1] = src_gray.at<uchar>(opposite_points[1]);
		
		/// find radius
		int8_t pos_blocked[] = {0,0}; /// point is on ellipse edge, waiting for opposie point
 		uint8_t stop = false;
		while(!stop){
			/// two oposite points
			for (int o = 0; o < 2; o++){	
				//~ printf("%d: %d",o, pos_blocked[o]);
				if (pos_blocked[o]){
					if (pos_blocked[o] >= blockation_limit*distance[o]){
						pos_blocked[o] = -1;
						//~ distance[(o+1)%2] = distance[o];
						distance[o] = distance[(o+1)%2];
					}
					pos_blocked[o] ++;
					opposite_points[o] = get_point_on_circle(corner, ang[o], distance[o], &index_out);
					floating_average[o] = (floating_average[o]*6 + src_gray.at<uchar>(opposite_points[o]))/7;	
					continue;
				}
				distance[o] += skip_size;
				float next_batch_mean = 0;
				for (int shift = skip_size; shift <= batch_size*skip_size; shift+=skip_size){
					Point2f point = get_point_on_circle(corner, ang[o], distance[o]+shift, &index_out);
					next_batch_mean += src_gray.at<uchar>(point);
				}
				next_batch_mean /= batch_size;
							
				opposite_points[o] = get_point_on_circle(corner, ang[o], distance[o], &index_out);
				floating_average[o] = (floating_average[o]*4 + src_gray.at<uchar>(opposite_points[o]))/5;
				
				#ifdef DEBUG
				if(!firstrun){
				Vec3b red(20,20,255);
				src.at<Vec3b>(opposite_points[o]) = red;
				}
				#endif
				
				int in_first_batch = (distance[o] - start_dist) < batch_size;
				if (abs(next_batch_mean - floating_average[o]) >= range_coeff){
					if(!in_first_batch && distance[o] > min_dist){
						pos_blocked[o] ++;
					}
				}
				if (index_out || distance[o] >= max_dist){
					pos_blocked[o] ++;
					stop = true;
				}
			}
			if (!index_out && pos_blocked[0] && pos_blocked[1] && distance[0] > min_dist+1 && distance[1]> min_dist+1 && distance[0]<max_dist && distance[1]<max_dist){
				critical_distances_temp[border_c].push_back(distance[0]);
				critical_distances_temp[(border_c+2)%4].push_back(distance[1]);
				critical_points_temp[border_c].push_back(opposite_points[0]);
				critical_points_temp[(border_c+2)%4].push_back(opposite_points[1]);
				#ifdef DEBUG
				Vec3b red(255,255,255);
				//~ circle( src, opposite_points[0], 1, red, 1, 0 );
				//~ circle( src, opposite_points[1], 1, red, 1, 0 );
				#endif
				stop = true;
			}
		}
	}
	
	/// joint temp. quadrant distances and point to ONE returnable vactor
	vector<Point2f> critical_points;
	critical_points.reserve(critical_points_temp[0].size() + critical_points_temp[1].size()+critical_points_temp[2].size()+critical_points_temp[3].size());
	critical_points.insert( critical_points.end(), critical_points_temp[0].begin(), critical_points_temp[0].end() );
	critical_points.insert( critical_points.end(), critical_points_temp[1].begin(), critical_points_temp[1].end() );
	critical_points.insert( critical_points.end(), critical_points_temp[2].begin(), critical_points_temp[2].end() );
	critical_points.insert( critical_points.end(), critical_points_temp[3].begin(), critical_points_temp[3].end() );
	
	critical_distances.reserve(critical_distances_temp[0].size() + critical_distances_temp[1].size()+critical_distances_temp[2].size()+critical_distances_temp[3].size());
	critical_distances.insert( critical_distances.end(), critical_distances_temp[0].begin(), critical_distances_temp[0].end() );
	critical_distances.insert( critical_distances.end(), critical_distances_temp[1].begin(), critical_distances_temp[1].end() );
	critical_distances.insert( critical_distances.end(), critical_distances_temp[2].begin(), critical_distances_temp[2].end() );
	critical_distances.insert( critical_distances.end(), critical_distances_temp[3].begin(), critical_distances_temp[3].end() );
	
	return critical_points;
}


Point2f get_point_on_circle(Point center, float ang, int radius, int* index_out){
	float x_diff = cos(ang)*(float)radius;
	float y_diff = sin(ang)*(float)radius;
	int x = center.x+(int)x_diff;
	int y = center.y+(int)y_diff;
	int x_lim = limit_pos(x,0, src_gray.cols);
	int y_lim = limit_pos(y,0, src_gray.rows);
	*index_out = x != x_lim || y != y_lim;
	return Point2f(x_lim, y_lim);
}
