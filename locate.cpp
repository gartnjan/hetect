#include "hetect.hpp"
#include <stdio.h>
#include <stdlib.h>
#include "opencv2/opencv.hpp"

//~ #define DEBUG 1

extern Mat src;
extern float target_radius;


Mat get_location(RotatedRect min_ellipse)
/// based on code examples from https://www.learnopencv.com/head-pose-estimation-using-opencv-and-dlib/
{	int points_n = 7;
	
	/// 3d points of the circle
	vector<Point3d> model_points;
	model_points.push_back(Point3d(0.0f, 0.0f, 0.0f)); /// center
	for (int i = 0; i < points_n; i++){
		double ang = 2*M_PI/points_n*i;
		double x = target_radius* sin(ang);
		double y = target_radius* cos(ang);
		model_points.push_back( Point3d(x, y, 0) ); /// point on the edge
	}

	/// 2d points of the elipse
	int a = min_ellipse.size.width /2+3;
	int b = min_ellipse.size.height /2+3;
	double theta = min_ellipse.angle/360*2*M_PI;
	float x0 = min_ellipse.center.x;
	float y0 = min_ellipse.center.y;

	vector<Point2d> image_points;
	image_points.push_back( Point2d(x0, y0) ); /// center
	
	for (int i = 0; i < points_n; i++){
		double ang = 2*M_PI/points_n*i;
		double x = x0 + a*cos(ang)*cos(theta) - b*sin(ang)*sin(theta);
		double y = y0 + a*cos(ang)*sin(theta) + b*sin(ang)*cos(theta);
		image_points.push_back( Point2d(x, y) ); ///  point on the edge
	}
	

	/// Camera internals
	Mat camera_matrix = (Mat_<double>(3,3) << 4.8935250771790834e+02, 0., 3.7166828259115147e+02, 0.,
    4.9002910883209307e+02, 2.3411380100214726e+02, 0., 0., 1.);
	Mat dist_coeffs = (Mat_<double>(1,5) <<  -1.0518017804094365e-02, 2.6722973996742182e-02,
    -1.6654577836637706e-03, 3.4813377352161017e-03, -3.2024717678039820e-02); 

	//~ cout << "Camera Matrix " << endl << camera_matrix << endl ;
	/// Output rotation and translation
	Mat rotation_vector; /// Rotation in axis-angle form
	Mat translation_vector;


	/// Solve for pose
	solvePnP(model_points, image_points, camera_matrix, dist_coeffs, rotation_vector, translation_vector);

	/// Project a 3D point (0, 0, 1000.0) onto the image plane.
	/// We use this to draw a line sticking out of the nose

	vector<Point3d> nose_end_point3D;
	vector<Point2d> nose_end_point2D;
	nose_end_point3D.push_back(Point3d(0,0,target_radius));
	nose_end_point3D.push_back(Point3d(0,0,-target_radius));

	projectPoints(nose_end_point3D, rotation_vector, translation_vector, camera_matrix, dist_coeffs, nose_end_point2D);

	#ifdef DEBUG
	for(size_t i=0; i < image_points.size(); i++) {
		circle(src, image_points[i], 3, Scalar(255,255,255), -1);
	}

	line(src,nose_end_point2D[0], nose_end_point2D[1], cv::Scalar(255,0,0), 2);
	
	//~ cout << "\nRotation Vector " << endl << rotation_vector << endl;
	//~ cout << "Translation Vector" << endl << translation_vector << endl;

	cout << "distance guess " << norm(translation_vector) << endl;

	//~ cout <<  nose_end_point2D << endl;
	#endif
	return translation_vector;
}
