#include "hetect.hpp"
#include <iomanip>
#include <time.h>
#include <iomanip> 

using namespace cv;
using namespace std;

/// corners detection
int CORNERS_LIMIT =  150;

/// corners filtering
#define CENTER_CIRC_RADIUS 2
//~ #define CENTER_CIRC_RADIUS 150
int AROUND_STEPS = 96;

//~ #define DEBUG 1
//~ #define DISP 1
extern int MASKING;

#define MCD 6 /// default min_corn_dist
#define DDA 100 /// defalut drift area, in near mode

distance_mode_t distance_mode = Far;
uint8_t detected_recently = false;
unsigned int short negative_detections = 3, negative_detections_lim = 1;
unsigned int short positive_detections = 0;
double min_corn_dist = MCD;

extern Source_t source ;
extern Mat src_gray, src;
extern ofstream logout;

int detected = false;
RotatedRect final_ellipse;
Point2f target;
int fps=30;
int drift_area = DDA; /// distance of new and old target (in pixels) used as mask for corners detection


void find_corners_shi() {
	const clock_t begin_time = clock();
	Mat position(Size(3,1), CV_64F);
	/// mask for searching corners
	vector<Point2f> corners;
	if (MASKING && detected_recently){
		Mat mask = Mat::zeros(src_gray.size(), CV_8U); 
		if (2*drift_area > src_gray.cols)
			drift_area = src_gray.cols/2;
		if (2*drift_area > src_gray.rows)
			drift_area = src_gray.rows/2;
		float rectx = limit_pos(target.x-drift_area,0,src_gray.cols-2*drift_area);
		float recty = limit_pos(target.y-drift_area,0,src_gray.rows-2*drift_area);
		//~ printf("x %f y %f, limited to %f %f, limit is %d\n", target.x-drift_area, target.y-drift_area, rectx, recty, src_gray.rows-2*drift_area);
		mask(Rect(rectx, recty,2*drift_area,2*drift_area)) = 1;
		corners = shi_tomasi_corners(CORNERS_LIMIT, min_corn_dist, mask);
		//~ #ifdef DEBUG
		src(Rect(rectx, recty,2*drift_area,2*drift_area)) = src(Rect(rectx, recty,2*drift_area,2*drift_area))+Vec3b(10,10,30);
		//~ #endif
	} else {
		corners = shi_tomasi_corners(CORNERS_LIMIT, min_corn_dist, Mat::ones(src_gray.size(), CV_8U));
	}
	detected = false;
	const clock_t corners_time = clock();

	vector<Point2f> corners_corrected = correct_corners(corners);
	//~ vector<Point2f> corners_corrected = corners;
	
	RotatedRect min_ellipse;
	Point2f target_temp;
	for( size_t i = 0; i < corners.size(); i++) {
		#ifdef DEBUG
		circle( src, corners_corrected[i], 1, Scalar(255, 5, 5), 1, 0  );
		//~ circle( src, corners_corrected[i], 1, Scalar(255, 255, 5), 1, 0  );
		#endif
		int score = 0;
		int biggest_chess;
		/// chessboard check
		int conv_shift;
		int color_range;
		int target_smalldist = 0;
		if (detected_recently && final_ellipse.size.width < final_ellipse.size.height)
			target_smalldist = final_ellipse.size.width;
		if (detected_recently && final_ellipse.size.width >= final_ellipse.size.height)
			target_smalldist = final_ellipse.size.height;
		if (chessboard_check(corners_corrected[i], &conv_shift, &color_range, detected_recently, target_smalldist, &biggest_chess)) {
			score ++;
			if (distance_mode == Near){ /// not corrected when not correcting all corners position
				score++;
			}
			//~ #ifdef DEBUG
			circle( src, corners_corrected[i], CENTER_CIRC_RADIUS, Scalar(15, 255, 20), 3, 0 );
			//~ #endif

		}
#ifdef DISP
		imshow( "Source image", src);
		waitKey(0);
#endif
		Point2f corner_corrected;
		//~ corner_corrected = correct_corner(src_gray,corners[i]);
		corner_corrected = corners_corrected[i];
		
		/// not in near, must search for ellipse
		if(score ==1) { 
			if(radial_check(corner_corrected, min_ellipse, conv_shift, color_range, biggest_chess)) {
				final_ellipse = min_ellipse;
				if(!MASKING)
					ellipse( src, min_ellipse, Scalar(255,20,255), 2, 8);
				score ++;
			}
		}

		/// final result
		if (score >= 2) {
			detected ++;
			target_temp = corner_corrected;
			if(MASKING && distance_mode == Far){
				break;
			}
		}
	}
	
	/// detected dealing
	if (detected == 1) {
		target = target_temp;
		negative_detections = 0;
		position = get_location(final_ellipse);
		if (positive_detections < 10000) {
			positive_detections++;
		}
	} else {
		positive_detections = 0;
		if (negative_detections < 10000)
			negative_detections ++;
		position = Mat::ones(3,1, CV_64F)*-1;
	}
	detected_recently = negative_detections <= negative_detections_lim-1;
	
	/// distance mode dealing
	if (MASKING) {
		/// swith to next mode 
		if(distance_mode == Near && detected > 1) {
			distance_mode = Far;
		} else if (distance_mode == Far && detected == 1 && (((final_ellipse.size.width+final_ellipse.size.height)/2 > src_gray.cols*0.5) || (final_ellipse.size.width+final_ellipse.size.height)/2 > src_gray.rows*0.5)) {
			distance_mode = Near;
		}
		if (distance_mode == Near && !detected && !detected_recently) {
			distance_mode = Far;
		}
		/// decisions based on actual mode
		if(distance_mode == Near) {
			putText(src, "NEAR MODE",Point(10,src.rows-20), FONT_HERSHEY_COMPLEX_SMALL, 1.0, Scalar(0,0,0), 3, CV_AA);
			putText(src, "NEAR MODE",Point(10,src.rows-20), FONT_HERSHEY_COMPLEX_SMALL, 1.0, Scalar(255,255,255), 1.5, CV_AA);
			min_corn_dist = 30;
			if(detected == 1){
				drift_area = DDA;
				circle( src, target, 20, Scalar(200, 20, 200), 6, 0 );
				position = Mat::ones(3,1, CV_64F)*-2;
				position.at<double>(Point(0,0)) = target.x-src_gray.cols/2.;
				position.at<double>(Point(1,0))= target.y-src_gray.rows/2.;
				position.at<double>(Point(2,0)) = (double) -1;
			}
		} else { /// FAR mode
			/// set min_corn_dist
			if (detected == 1){
			//~ if (detected_recently){
				drift_area = (final_ellipse.size.width + final_ellipse.size.height)/2+src_gray.rows/10;
				ellipse( src, final_ellipse, Scalar(255,20,255), 2, 8 );
			}
			if (detected_recently){
				if (final_ellipse.size.width < final_ellipse.size.height){
					min_corn_dist = final_ellipse.size.width*0.2;
				} else {
					min_corn_dist = final_ellipse.size.height*0.2;
				}
			} else {
				min_corn_dist = MCD;
			}
			CORNERS_LIMIT = set_corners_limit(detected);
		}
	}
	
	/// time measurements
	const clock_t end_time = clock();
	cout << "corn. " << setw(3)<< int(float( corners_time - begin_time ) /  CLOCKS_PER_SEC *1000) << " ms, "
	 << "heur. " << setw(3) << int(float( end_time - corners_time ) /  CLOCKS_PER_SEC *1000) << " ms - tot. "
	 << setw(3) << int(float( end_time - begin_time ) /  CLOCKS_PER_SEC *1000 ) << "ms " << "with " << setw(3)<< CORNERS_LIMIT<< " corners limit,"
	 //~ if (detected == 1)
		//~ cout << ", detected at " << target.x << " " << target.y;
	  << " position " << position.at<double>(0,0) <<" " << position.at<double>(1,0) <<" " << position.at<double>(2,0) 
	  //~ << position
	  << "\r\n" << flush;
	 
	 logout << setw(8) <<(detected==1) << ", "<< setw(8) << (distance_mode==Near) << ", "
	 << setw(8)<< (float( corners_time - begin_time ) /  CLOCKS_PER_SEC *1000) << ", "
	 << setw(8) << (float( end_time - corners_time ) /  CLOCKS_PER_SEC *1000) << ",  "
	 << setw(8) << (float( end_time - begin_time ) /  CLOCKS_PER_SEC *1000 ) << ", "
	 << setw(8)<< CORNERS_LIMIT << ", "
	 << setw(10)<< setprecision(8)  << position.at<double>(0,0)/2 << ", "<< setw(10)<< setprecision(8) << position.at<double>(1,0)/2<< ", "<< setw(10) << setprecision(8) << position.at<double>(2,0)/2 << ", "
	 << "\r\n" << flush;
	
	if (source == Webcam){
		fps = (int)round((9*fps+1./(double( end_time - begin_time ) /  CLOCKS_PER_SEC ))/10);
	} else {
		fps = (int)round(1./(double( end_time - begin_time ) /  CLOCKS_PER_SEC ));
	}
	if (MASKING) {
		stringstream corners_num_s;
		corners_num_s << setw(3) << fps << " FPS";
		putText(src, corners_num_s.str(), Point(10,src.rows-40), FONT_HERSHEY_COMPLEX_SMALL, 1.0, Scalar(0,0,0), 3, CV_AA);
		putText(src, corners_num_s.str(), Point(10,src.rows-40), FONT_HERSHEY_COMPLEX_SMALL, 1.0, Scalar(255,255,255), 1.5, CV_AA);
	}
	
}


Point2f correct_corner(Point2f corner) {
	int steps = 8; /// points on which i look for black and white values
	int start_dist = 5; /// radius on which i look for black and white values
	int shift = 3; /// maximal corner shift
	/// cenetr shift
	Point2f new_corner;
	int corner_err_best = 0xFFF0;
	for (int x_s = -shift; x_s <= shift; x_s++) {
		for (int y_s = -shift; y_s <= shift; y_s++) {
			/// corner accuracy
			int corner_err = 0;
			Point2f center(corner.x+x_s, corner.y+y_s);
			for (int step = 0; step <= steps/2; step++) {
				int index_out;
				float ang = 2*M_PI / steps * step;
				Point2f point1 = get_point_on_circle(center, ang, start_dist, &index_out);
				ang += M_PI;
				Point2f point2 = get_point_on_circle(center, ang, start_dist, &index_out);
				corner_err += abs(src_gray.at<uchar>(point1) - src_gray.at<uchar>(point2));
			}
			if (corner_err < corner_err_best) {
				corner_err_best = corner_err;
				new_corner = center;
			}
		}
	}
	return new_corner;
}


vector<Point2f> correct_corners(vector<Point2f> corners) {
	int steps = 8; /// points on which i look for black and white values
	int start_dist = 5; /// radius on which i look for black and white values
	int shift  = 3; /// maximal corner shift
	//~ if (distance_mode == Far){
		//~ shift = 3;
	//~ } else {
		//~ shift = 6;
	//~ }
	vector<Point2f> correct_corners;
	for( size_t c = 0; c < corners.size(); c++) {
		/// cenetr shift
		int corner_err_best = 0xFFF0;
		Point2f new_corner = corners[c];
		for (int x_s = -shift; x_s <= shift; x_s++) {
			for (int y_s = -shift; y_s <= shift; y_s++) {
				/// corner accuracy
				int corner_err = 0;
				Point2f center(corners[c].x+x_s, corners[c].y+y_s);
				for (int step = 0; step <= steps/2; step++) {
					int index_out;
					float ang = 2*M_PI / steps * step;
					Point2f point1 = get_point_on_circle(center, ang, start_dist, &index_out);
					ang += M_PI;
					Point2f point2 = get_point_on_circle(center, ang, start_dist, &index_out);
					corner_err += abs(src_gray.at<uchar>(point1) - src_gray.at<uchar>(point2));
				}
				if (corner_err < corner_err_best) {
					corner_err_best = corner_err;
					new_corner = center;
				}
			}
		}
		correct_corners.push_back(new_corner);
	}
	return correct_corners;
}


vector<Point2f> shi_tomasi_corners(int maxCorners, double min_dist, Mat mask) {

	/// detector parameters
	double qualityLevel = 0.0001; 	/// compared to the best found corner					/// region of interest
	int blockSize = 3;
	bool useHarrisDetector = false;
	double k = 0.04;					/// Harris shit

	vector<Point2f> corners;

	goodFeaturesToTrack( src_gray, corners, maxCorners, qualityLevel,
						 min_dist, mask, blockSize, useHarrisDetector, k );

	return corners;
}


int set_corners_limit(int detected) {
	int corners_min = 20;
	int corners_max = 250;
	int change_cycle = 1; /// after how many negative or positive detections aply changes
	if (detected != 1) {
		if (!detected_recently){
			if (CORNERS_LIMIT+30 <= corners_max)
				return CORNERS_LIMIT+30;
			return corners_max;
		}
		if (CORNERS_LIMIT+15 <= corners_max)
			return CORNERS_LIMIT+15;
	} else if (positive_detections % change_cycle == change_cycle-1) {
		if (CORNERS_LIMIT-5 >= corners_min){
			return CORNERS_LIMIT-5;
		}
	}
	return CORNERS_LIMIT;
}
