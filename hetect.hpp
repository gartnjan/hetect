#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <math.h>
#include <fstream>

using namespace cv;
using namespace std;

#define _USE_MATH_DEFINES

enum Source_t {Webcam, Folder};
enum distance_mode_t {Near, Far};

/// corners.cpp
void find_corners_shi();
vector<Point2f> shi_tomasi_corners(int maxCorners, double min_dist, Mat mask);
vector<Point2f> correct_corners(vector<Point2f> corners);
Point2f correct_corner(Point2f corner);
int set_corners_limit(int detected);


/// chessboard.cpp
int chessboard_check(Point2f center, int* conv_shift, int* color_range, uint8_t detected_recently, int target_size, int* biggest_chess);
int convolution_test(vector<uint8_t> color_values, int mean_color, int* shift);


/// radial.cpp
int radial_check(Point2f corner, RotatedRect& ellipse, int conv_shift, int color_range, int biggest_chess);
vector<Point2f>  get_critical_distances(vector<int>& new_distances, Point2f corner, int conv_shift, int min_dist, int* possible_distances, int color_range, int firstrun);
int ellipse_test(vector<Point2f> valid_critical_points, vector<Point2f> points_to_test, RotatedRect& min_ellipse, float ellipse_limit, vector<Point2f>& new_valid_crdst );
Point2f get_point_on_circle(Point center, float ang, int radius, int* index_out);


/// utils.cpp
double count_median(vector<int> scores, float pos);
float limit_pos(float pos, int down, int size);
int continuity_filter(vector<int> critical_distances, vector<int8_t>& critical_point_valid, int* start_at);
template<typename T>
void printVector(const T& t);
template<typename T>
void printVectorInVector(const T& t);

/// locate.cpp
Mat get_location(RotatedRect min_ellipse);

/// main.cpp
char* getCmdOption(char ** begin, char ** end, const std::string & option);
bool cmdOptionExists(char** begin, char** end, const std::string& option);
