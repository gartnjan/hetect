#include "hetect.hpp"
#include <stdio.h>
#include <stdlib.h>

extern int AROUND_STEPS, MASKING;
extern distance_mode_t distance_mode;
extern Mat src_gray, src;
int conv_step = 3;

//~ #define DEBUG

int chessboard_check(Point2f center, int* conv_shift, int* color_range, uint8_t detected_recently, int target_size, int* biggest_chess) {
	/// returns if corner is a chessboard corner, otherwise 0
	int steps = AROUND_STEPS/conv_step;
	float radiuses_limit; /// how many radiuses must be right
	int min_diff_coeff = 30; /// minimal difference between white and black
	//~ int shift_variance = 0.2*steps; /// convolutional shifts on valid radiuses must be lower than this 
	uint8_t shift_limit = 3; /// huw much can vary conv. shift between radiuses
	vector<int> radiuses;
	if (distance_mode == Far){
		if (MASKING && detected_recently && target_size >= 40){
			radiuses =  {(int)(target_size*0.1), (int)(target_size*0.15), (int)(target_size*0.2),
				(int)(target_size*0.25), (int)(target_size*0.3), (int)(target_size*0.4)};
			radiuses_limit = 5;
		} else {
			radiuses =  {3,4,5,6,7,8,12,14,16};
			radiuses_limit = 6;
		}
	} else { /// near mode
		radiuses = {20,25,30,35,40,50};
		radiuses_limit = 5;
	}
	//~ vector<int> radiuses {11,12,15,20,25,30};
	/// iterate over different distances
	int radiuses_n = (int)radiuses.size();
	int radiuses_ok = 0;
	int last_valid_shift = -1;
	uint8_t fixed_once = false;
	//~ printf("------------\n");
	*color_range = 0;
	for (int dist = 0; dist < radiuses_n; dist++) {
		vector<uint8_t> color_values; 
		int radius_out= false;
		int radius = radiuses[dist];
		int mean_color = 0, max_color = 0, min_color = 255;
		Point2i last_point(-1,-1);
		for (int step = 0; step < steps; step++) {
			float ang = 2*M_PI / steps * step + ( 2*M_PI/steps/radiuses_n*dist );
			int index_out;
			Point2i point = get_point_on_circle(center, ang, radius, &index_out);
			if (point == last_point){
				continue;
			}
			last_point = point;
			color_values.push_back(src_gray.at<uchar>(point));
			//~ mean_color += src_gray.at<uchar>(point); 	/// get mean color
			if(src_gray.at<uchar>(point) < min_color) {	 /// save extreme values
				min_color = src_gray.at<uchar>(point);
			} else if (src_gray.at<uchar>(point) > max_color) {
				max_color = src_gray.at<uchar>(point);
			}
			//~ #ifdef DEBUG
			//~ Vec3b white(255,255,255);
			//~ src.at<Vec3b>(point) = white;
			//~ #endif
			if(index_out){
				radius_out = true;
			}
		}
		if (radius_out){
			continue;
		}
		//~ mean_color /= steps;
		int color_range_ = max_color - min_color;
		int mid_color = min_color + color_range_/2;
		mean_color = mid_color; // TRYING
		//~ cout << " - ";

		if(color_range_ < min_diff_coeff) {
			continue; /// try another radius
		}
		int convolution_shift;
		if(convolution_test(color_values, mean_color, &convolution_shift)){
			/// dealing with half circle skipping
			uint8_t near_last_valid = (last_valid_shift >= 0  && abs(convolution_shift - last_valid_shift) <= shift_limit)
											 || (last_valid_shift >= 0  && abs(convolution_shift - last_valid_shift) >= steps/2. - shift_limit);
			if (last_valid_shift < 0){
				radiuses_ok ++;
				* biggest_chess = radius;
				*color_range = *color_range + color_range_;
				#ifdef DEBUG
				circle( src, center, radius, Scalar(255, 255, 255), 1, 0 );
				#endif
				last_valid_shift = convolution_shift;
				fixed_once = true;
			} else if (last_valid_shift >= 0 && near_last_valid){
				radiuses_ok ++;
				* biggest_chess = radius;
				*color_range = *color_range + color_range_;
				#ifdef DEBUG
				circle( src, center, radius, Scalar(255, 255, 255), 1, 0 );
				#endif
				last_valid_shift = convolution_shift;
				fixed_once = false;
			} else {
				#ifdef DEBUG
				circle( src, center, radius, Scalar(0, 255, 255), 1, 0 );
				#endif
				if (fixed_once){
					last_valid_shift = convolution_shift;
				}
			}
			//~ printf("%d ", convolution_shift);
		}
	}
	if (radiuses_ok >= radiuses_limit){
		*color_range = *color_range / radiuses_ok;
		*conv_shift = last_valid_shift*conv_step; /// returns mean convolution shift to int
		return true;
	} else {
		return false;
	}
}


int convolution_test(vector<uint8_t> color_values, int mean_color, int* final_shift){ 
	float points_ok_limit = 0.9; /// how many points must have right color
	/// generate patterns for thee side views
	int steps = color_values.size();
	uint8_t pattern_values[steps]; /// white-black-white-black from top
	for (int i = 0; i < steps; i++){
		if (i < steps/24.*5 || (i > steps/24.*13 && i < steps/24.*17 )){
			pattern_values[i] = 255;
		} else if ((i >= steps/24.*5 && i <= steps/24.*7) || (i >= steps/24.*11 && i <= steps/24.*13) || (i >= steps/24.*17 && i <= steps/24.*19)){
			pattern_values[i] = 128;
		} else {
			pattern_values[i] = 0;
		}
	}
	
	/// find convolution maximum for each mask (side view)
	int max_shift;
	int max_corelation = 0;
	for (int shift = 0; shift < steps/2.; shift++){
		int corelation = 0;
		for (int i = 0; i < steps; i++){
			corelation += color_values[(i+shift)%steps] * pattern_values[i];
		}
		if (corelation > max_corelation){
			max_corelation = corelation;
			max_shift = shift;
		}
	}
	
	/// test quadrants
	int points_ok = 0;
	for (int i = 0; i < steps; i++){
		#ifdef DISP
		printf("%d ", color_values[(i + max_shifts[m])%steps]);
		#endif
		uint8_t color_white = color_values[(i + max_shift)%steps] > mean_color;
		uint8_t pattern_white = pattern_values[i] == 255;
		uint8_t pattern_black = pattern_values[i] == 0;
		if ((color_white && !pattern_black) || (!color_white && !pattern_white)){
			points_ok ++;
		}
	}

	#ifdef DISP
	printf("\nmean %d\n", mean_color);
	#endif
	
	/// find best mask (view angle)


	//~ if (result)
		//~ printf("mask %d, shift%d, na 2 %d %d %d\n", best_mask, *final_shift, max_corelations[0], max_corelations[1], max_corelations[2]);
	//~ if (result){
		//~ *conv_mask = best_mask;
		//~ printf("mask set to %d\n", best_mask);
	//~ }
	*final_shift = max_shift;
	return points_ok >= steps*points_ok_limit;
	
}
