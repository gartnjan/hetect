#include "hetect.hpp"
#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdio.h>
#include <stdlib.h>


//~ #define DEBUG

int continuity_filter(vector<int> critical_distances, vector<int8_t>& critical_point_valid, int* start_at) {
	float continuity_limit = 0.2; /// how much can vary two neigh. critical distances to be valid
	int continuity_limit_abs = 2;
	int min_in_row = 5;
	
	critical_point_valid.resize(critical_distances.size());
	
	#ifdef DEBUG
	cout << "distances: \n";
	
	for (size_t e = 0; e < critical_distances.size(); e++){
		cout << (unsigned)critical_distances[e] << " ";
	}
	cout << "\n";
	#endif
	
	
	/// find few critical distances in area of continuity
	int distances_ok = 0;
	/// first few valid distances
	int first_valid = -1;
	int steps = critical_distances.size();
	int diff;
	//~ int diff_last;
	int8_t diff_valid = false;
	for (int i = *start_at; i < steps+*start_at; i++){
		int in_row = 0;
		for (int e = 0; e < steps; e++){
			if (critical_distances[(i+e)%steps] < 0)
				break;
			if (diff_valid){
				//~ diff_last = diff;
			} else {
				//~ diff_last = 0;
				diff_valid = 1;
			}
			int next_dist = critical_distances[(i+e+1)%steps];
			diff = next_dist - critical_distances[(i+e)%steps];
			int smaller_one;
			if (critical_distances[i%steps] < critical_distances[(i+1)%steps]){
				smaller_one = critical_distances[i%steps];
			} else {
				smaller_one = critical_distances[(i+1)%steps];
			}
				
			if (abs(diff) > smaller_one*continuity_limit+continuity_limit_abs){
				break;
			}
			in_row ++;
			if (in_row == min_in_row){
				first_valid = i%steps;
				break;
			}
			
		}
		if (first_valid >= 0){
			break;
		}
	}
	if (first_valid<0){
		for (size_t e = 0; e < critical_point_valid.size(); e++){
			critical_point_valid[e] = false;
		}
		return 0;
	}
	
	#ifdef DEBUG
	cout << "first valid" << first_valid << "\n";
	#endif
	
	/// the continuity filter
	*start_at = -1;
	distances_ok += min_in_row;
	for (int i = first_valid; i <= first_valid + min_in_row; i++){
		critical_point_valid[(i)%steps] = 1;
	}
	int last_valid_dist = critical_distances[(first_valid+min_in_row)%steps];
	int last_dist = critical_distances[(first_valid+min_in_row)%steps];
	diff_valid = false;
	int in_row = 0;
	for (int i = first_valid+min_in_row+1; i < first_valid+steps; i++){
		if (diff_valid){
			//~ diff_last = diff;
		} else {
			//~ diff_last = 0;
			diff_valid = 1;
		}
		int next_dist = critical_distances[(i)%steps];
		diff = next_dist - last_valid_dist;
		int smaller_one;
		if (last_valid_dist < next_dist){
			smaller_one = last_valid_dist;
		} else {
			smaller_one = next_dist;
		}
		if (next_dist > 0 && (abs(diff) <= smaller_one*continuity_limit+continuity_limit_abs || in_row >= min_in_row )){
			critical_point_valid[(i)%steps] = 1;
			last_valid_dist = next_dist;
			distances_ok ++;
			in_row = 0;
		} else if (next_dist > 0){
			critical_point_valid[(i)%steps] = false;
			if(*start_at < 0)
				*start_at = i%steps;
		} else {
			critical_point_valid[(i)%steps] = -1;
		}
		/// jump back to valids
		int straight_diff = last_dist - next_dist;
		if (last_dist < next_dist){
			smaller_one = last_dist;
		} else {
			smaller_one = next_dist;
		}
		if (abs(straight_diff) <= smaller_one*continuity_limit+continuity_limit_abs){
			in_row++;
		}
		
	}
	
	#ifdef DEBUG
	for (size_t e = 0; e < critical_point_valid.size(); e++){
		cout << " "<<(unsigned)critical_point_valid[e] << " ";
	}
	cout << "\n";
	#endif
	
	return distances_ok;
}



float limit_pos(float pos, int down, int size) {
	if (!size)
		return 0;
	if (pos >= size) {
		return size-1;
	}
	if (pos <= down) {
		return down;
	}
	return pos;
}


double count_median(vector<int> scores, float pos) {
	sort(scores.begin(), scores.end());
	if (!scores.size())
		return 0;
	return scores[round((scores.size()-1) * pos)];
}

template<typename T>
void printVector(const T& t) {
    copy(t.cbegin(), t.cend(), ostream_iterator<typename T::value_type>(std::cout, ", "));
}

template<typename T>
void printVectorInVector(const T& t) {
    std::for_each(t.cbegin(), t.cend(), printVector<typename T::value_type>);
}
